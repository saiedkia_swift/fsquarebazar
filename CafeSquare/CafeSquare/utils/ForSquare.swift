//
//  ForSquare.swift
//  CafeSquare
//
//  Created by sk on 5/1/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import Foundation
import SwiftyJSON

fileprivate struct  Constants {
    static let client_id = "S1XUO0JAO4J02OCOFUZ21FDNMSRA0CX5SV3PSAAYDNGQ5QSK"
    static let client_secret = "PMERSMJKLZD1OWDQP0YF1PO0M5NGG2B0YNCOTQHUTKPHBWC0"
}

extension GoogleMapView{
    func search(lat:Double, long:Double, resultCallback:@escaping (_ json:JSON, _ succed:Bool) -> Void){
        let net = Network()
        net.setContentType(.UrlEncoded)
        let parameters = [
            "client_id" : Constants.client_id,
            "client_secret" : Constants.client_secret,
            "v" : "20190202",
            "ll" : "\(lat),\(long)",
            "llAcc" : "10000.0",
            "radius" : "500",
            "limit" : "15"
        ]
        
        net.get(route: "https://api.foursquare.com/v2/venues/search", parameters: parameters, callback: {(json, status) in
            switch status{
            case .success:
                resultCallback(json!, true)
                break
            default:
                resultCallback(JSON.null, false)
                break
                }
            })
        }
    
        func search(searchKey:String, lat:Double, long:Double, resultCallback:@escaping (_ json:JSON, _ succed:Bool) -> Void){
            let net = Network()
            net.setContentType(.UrlEncoded)
            let parameters = [
                "client_id" : Constants.client_id,
                "client_secret" : Constants.client_secret,
                "v" : "20190202",
                "query" : searchKey,
                "ll" : "\(lat),\(long)",
                "llAcc" : "10000.0",
                "radius" : "500",
                "limit" : "15"
            ]
            
            net.get(route: "https://api.foursquare.com/v2/venues/search", parameters: parameters, callback: {(json, status) in
                switch status{
                case .success:
                    resultCallback(json!, true)
                    break
                default:
                    resultCallback(JSON.null, false)
                    break
                }
            })
        }
}
