//
//  Alert.swift
//  CafeSquare
//
//  Created by sk on 5/1/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import Foundation
import UIKit

protocol Alert {
    func showAlert(message:String, btnTitle:String, callBack: (()-> Void)?)
    func showAlert(message:String, btnOneTitle:String, btnTwoTitle:String, callBackOne: @escaping ()-> Void, callBackTwo: @escaping ()-> Void, cancelCallback: (() -> Void)?)
}

extension Alert {
    func showAlert(message:String, btnTitle:String, callBack: (()-> Void)?){
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: btnTitle, style: .default, handler: {(uiAlert) in callBack?()}))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    
    func showAlert(message:String, btnOneTitle:String, btnTwoTitle:String, callBackOne: @escaping ()-> Void, callBackTwo: @escaping ()-> Void, cancelCallback: (() -> Void)?){
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: btnTwoTitle, style: .default, handler: {(uiAlert) in callBackTwo()}))
        
        alert.addAction(UIAlertAction(title: btnOneTitle, style: .default, handler: {(uiAlert) in callBackOne()}))
        
        if cancelCallback != nil{
            alert.addAction(UIAlertAction(title: "انصراف", style: .cancel, handler: {(uiAlert) in cancelCallback?()}))
        }
        
        
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
}

extension UIView{
    
    private struct dataStoreage {
        static var actionKey = "tapAction"
    }
    
    private var tapGestureRecognizerAction: (() -> Void)?{
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &dataStoreage.actionKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &dataStoreage.actionKey) as? (() -> Void)
            return tapGestureRecognizerActionInstance
        }
    }
    
    func addTapGestureRecognizer(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(invoke))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc private func invoke(){
        tapGestureRecognizerAction?()
    }
    
    func clearSubViews(){
        for item in subviews{
            item.removeFromSuperview()
        }
    }
}

extension Substring{
    var value:String {
        return String(self)
    }
}

extension UIViewController: Alert{
    
}
