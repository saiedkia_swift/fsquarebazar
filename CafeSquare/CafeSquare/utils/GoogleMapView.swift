//
//  MapFuncs.swift
//  CafeSquare
//
//  Created by sk on 5/1/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import Foundation
import GoogleMaps
import MapKit


protocol GoogleMapViewDelegate {
    func googleMapView(status: LocationAuthorizationStatus)
    func googleMapView(locations: [CLLocation], isCurrentLocation:Bool)
    func googleMapView(lat:Double, long:Double, zoom:Float)
    func googleMapView(didtap marker:GMSMarker)
}


extension GoogleMapViewDelegate{
    func googleMapView(status: LocationAuthorizationStatus){}
    func googleMapView(locations: [CLLocation]){}
    func googleMapView(lat:Double, long:Double, zoom:Float){}
    func googleMapView(didtap marker:GMSMarker){}
}

class GoogleMapView : UIView, CLLocationManagerDelegate, GMSMapViewDelegate{
    var delegate:GoogleMapViewDelegate?
    var map:GMSMapView!
    private(set) var locationManager:CLLocationManager?
    private var isCurrentLocation = true
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        map = GMSMapView(frame: self.frame)
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        map.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        map = GMSMapView(frame: self.frame)
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        map.delegate = self
    }
    
    override func layoutSubviews() {
        if subviews.count == 0{
            map?.frame = self.bounds
            self.addSubview(map!)
        }
    }
    
    var authorizationStatus:LocationAuthorizationStatus  {
        switch CLLocationManager.authorizationStatus(){
        case .denied:
            return LocationAuthorizationStatus.denied
        case .authorizedWhenInUse,.authorizedAlways:
            return LocationAuthorizationStatus.granted
        case .restricted, .notDetermined:
            return LocationAuthorizationStatus.notSet
        }
    }
    
    func changeMapType(_ type: GMSMapViewType) {
        map?.mapType = type
    }
    
    func findMe(_ zoom:Int = 8){
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.startUpdatingLocation()
    }
    
    
    func addMarker(Latitude:Double, longitude:Double, zoom:Float, MarkerName:Int, imgurl:String? = nil, tag:String? = nil, data:Any? = nil, title:String? = nil, snippet:String? = nil) {
        
        let position = CLLocationCoordinate2D(latitude: Latitude, longitude: longitude)
        let marker = GMSMarker(position: position)
        marker.tracksViewChanges = true
        marker.snippet = snippet
        marker.title = title
        marker.appearAnimation = .pop
        marker.userData = data
        
        marker.map = map
    }
    
    func clearMarkers(){
        map.clear()
    }
    
    func zoomToLocation(Latitude:Double, longitude:Double, zoom:Float) {
        CATransaction.begin()
        CATransaction.setValue(2, forKey: kCATransactionAnimationDuration)
        let camera  = GMSCameraPosition.camera(withLatitude: Latitude, longitude: longitude, zoom: zoom)
        
        self.map?.animate(to: camera)
        CATransaction.commit()
    }
    
    func requestPermission(){
        locationManager?.requestAlwaysAuthorization()
    }
    
    func openSettings(){
        let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)! as URL
        UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        delegate?.googleMapView(status: authorizationStatus)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        delegate?.googleMapView(locations: locations, isCurrentLocation:isCurrentLocation)
        isCurrentLocation = false
        
        manager.stopUpdatingLocation()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        delegate?.googleMapView(lat: position.target.latitude, long: position.target.longitude, zoom: position.zoom)
    }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        delegate?.googleMapView(didtap: marker)
        return false
    }
}

enum LocationAuthorizationStatus{
    case notSet
    case granted
    case denied
}

enum permissionType{
    case always
    case inApp
}
