//
//  DataBase.swift
//  CafeSquare
//
//  Created by sk on 5/5/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class DataBase {
    private static var _default:Realm?
    static var `default`:Realm{
        get{
            if _default == nil{
                _default = try! Realm()
            }
        
            return _default!
        }
    }
}
