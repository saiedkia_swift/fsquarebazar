//
//  BaseViewController.swift
//  SimpleQ
//
//  Created by sk on 4/20/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController : UIViewController{
    
    override func viewDidLoad() {
        hideKeyboarTappedAround()
    
        NotificationCenter.default.addObserver(self, selector: #selector(dissmissKeyboard), name: NSNotification.Name("dissmissKeyboard"), object: nil)
    }
    
    private func hideKeyboarTappedAround(){
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dissmissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dissmissKeyboard(_:NSNotification?){
            self.view.endEditing(true)
    }
    
    func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
