//
//  DrawerViewController.swift
//  CafeSquare
//
//  Created by sk on 5/4/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import UIKit

class DrawerViewController: UIViewController, Drawer {
    @IBOutlet weak var lView: UIView!
    var width:CGFloat = 320
    @IBOutlet weak var rbFindMe: UISwitch!
    var drawerView: UIView? {
        get{
            return self.view
        }
    }
    
    private var _container:UIView?
    var container: UIView? {
        get {
            return _container //view.superview
        }set{
            _container = newValue
            
            view.frame = CGRect(x: UIScreen.main.bounds.width, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            if container?.superview == nil{
                container?.addSubview(view)
                container?.bringSubviewToFront(view)
            }else{
               container?.superview?.addSubview(view)
                container?.superview?.bringSubviewToFront(view)
            }
            
        }
    }
    var findMe: Bool{
        get {
            return  rbFindMe.isOn
        }set {
            rbFindMe.isOn = newValue
        }
    }
    
    var isOpen:Bool = false
    var isOnline:Bool {
        get{
            return rbFindMe.isOn
        }set{
            rbFindMe.isOn = newValue
            UserDefaults.standard.set(newValue, forKey: "isOnline")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lView.addTapGestureRecognizer(action: {
            self.toggle()
        })
        
        
        
        if let isOnline = UserDefaults.standard.object(forKey: "isOnline"){
            self.isOnline = isOnline as! Bool
        }else{
            UserDefaults.standard.set(true, forKey: "isOnline")
        }
        
        rbFindMe.addTarget(self, action: #selector(rbValueChaged), for: .valueChanged)
    }
    
    @objc func rbValueChaged(mySwitch: UISwitch){
        isOnline = rbFindMe.isOn
    }
    

    func toggle(){
        if !isOpen{
            UIView.animate(withDuration: 0.5, animations: {
                self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            })
            isOpen = true
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.view.frame = CGRect(x: UIScreen.main.bounds.width, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            })
            isOpen = false
        }
    }

}


protocol Drawer {
    var isOnline:Bool {get set }
    var width:CGFloat { get set }
    var isOpen:Bool { get set }
    var drawerView:UIView? {get }
    var container:UIView? { get set}
    var findMe:Bool { get set }
    func toggle()
}
