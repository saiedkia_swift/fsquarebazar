//
//  Models.swift
//  CafeSquare
//
//  Created by sk on 5/5/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import Foundation
import RealmSwift

class RawData : Object{
    @objc dynamic var data:String = ""
    static func build<T:RawData>(data:String) -> T {
        let raw = T()
        raw.data = data
        return raw
    }
}


class Pin : RawData{
}

class Location : Object{
    @objc dynamic var lat:Double = 0
    @objc dynamic var lon:Double = 0
}

