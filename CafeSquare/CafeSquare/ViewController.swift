//
//  ViewController.swift
//  CafeSquare
//
//  Created by sk on 5/1/19.
//  Copyright © 2019 saiedkia. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON

class ViewController: BaseViewController, GoogleMapViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var vMap: GoogleMapView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var imgMenu: UIImageView!
    var drawer:Drawer!
    
    private var lat:Double = 0
    private var lon:Double = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtSearch.delegate = self
        
        vMap.map.settings.myLocationButton = true
        vMap.map.isMyLocationEnabled = true
        vMap.delegate = self
        
        let arrow = UIImageView(image: UIImage(named: "arrow")!)
        arrow.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        txtSearch.leftView = arrow
        
        arrow.addTapGestureRecognizer(action: {
        })
        
        imgMenu.addTapGestureRecognizer(action: {
            self.dismissKeyboard()
            self.drawer.toggle()
        })
       
        // drawer
        let drawerController = storyboard?.instantiateViewController(withIdentifier: "DrawerViewController") as! DrawerViewController
                drawer = drawerController
        drawer.container = self.view
        
        
        
        loadPreviousPins()
        
    }
    
    private func loadPreviousPins(){
        if let location = DataBase.default.objects(Location.self).first{
            vMap.zoomToLocation(Latitude: location.lat, longitude: location.lon, zoom: 15)
            
            let pins = DataBase.default.objects(Pin.self)
            for pin in pins{
                let json = JSON(parseJSON: pin.data)
                vMap.addMarker(Latitude: json["location"]["lat"].doubleValue, longitude: json["location"]["lng"].doubleValue, zoom: 0, MarkerName: 1, tag: json["id"].stringValue)
            }
        }
    }
    
    func googleMapView(status: LocationAuthorizationStatus) {
        if status == .granted{
            vMap.findMe()
        }else if status == .denied{
            showAlert(message: "We need to know your location, help us to help you..", btnOneTitle: "Help me", btnTwoTitle: "no", callBackOne: {
                self.vMap.openSettings()
            }, callBackTwo: {}, cancelCallback: nil)
        }
    }
    
    func googleMapView(locations: [CLLocation], isCurrentLocation:Bool) {
        guard  let corinate = locations.first else {
            return
        }
        
        lat = corinate.coordinate.latitude
        lon = corinate.coordinate.longitude
        
        if isCurrentLocation{
            vMap.zoomToLocation(Latitude: lat, longitude: lon, zoom: 15)
            if drawer.isOnline {
                vMap.search(lat: lat, long: lon, resultCallback: {(json, status) in
                    if status{
                        self.vMap.clearMarkers()
                        self.addMarkers(json: json)
                    }
                })
            }
        }else{
            
        }
        
        let locations = DataBase.default.objects(Location.self)
        try! DataBase.default.write {
            DataBase.default.delete(locations)
            let location = Location()
            location.lat = lat
            location.lon = lon
            DataBase.default.add(location)
        }
    }
    
    func googleMapView(didtap marker: GMSMarker) {
        //print(marker.userData)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = txtSearch.text, text.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines) != ""{
            vMap.search(searchKey: text, lat: self.lat, long: self.lon, resultCallback: {(json, status) in
                if status{
                    self.vMap.clearMarkers()
                    self.addMarkers(json: json)
                    self.dismissKeyboard()
                }else{
                    self.showAlert(message: "load data failed!!", btnTitle: "ok", callBack: nil)
                }
            })
        }
        
        return true
    }
    
    
    private func addMarkers(json:JSON){
        let venues = json["response"]["venues"].arrayValue
        
        let pins = DataBase.default.objects(Pin.self)
        try! DataBase.default.write {
            DataBase.default.delete(pins)
            DataBase.default.add(venues.map({x in let tmpPin:Pin = Pin.build(data: x.rawString()!)
                return tmpPin
            }))
        }
        
        for venue in venues{
            if let lat = venue["location"]["lat"].double, let lng = venue["location"]["lng"].double{
                vMap.addMarker(Latitude: lat, longitude: lng, zoom: 0, MarkerName: 1, tag: venue["id"].string, title: venue["name"].stringValue, snippet: venue["name"].stringValue)
            }
        }
    }
}
